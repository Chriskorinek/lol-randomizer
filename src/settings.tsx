import React, {useState} from "react";

export type SettingsDef = {
    numberOfPlayers: number,
    lane: boolean,
    champions: boolean,
    items: boolean
}

export type SettingsProps = {
    onChange: (settings: SettingsDef) => void
    settings: SettingsDef
}

const Settings = ({onChange, settings}: SettingsProps) => {

    return <div>
        <h2>settings</h2>
        <label>
            number of players

            <input type='number' value={settings.numberOfPlayers}
                   min={1}
                   max={5}
                   onInput={e => onChange({...settings, numberOfPlayers: Number(e.currentTarget.value)})}/>
        </label>

        {/*<label>*/}
        {/*    champion*/}

        {/*    <input type='checkbox'/>*/}
        {/*</label>*/}

        {/*<label>*/}
        {/*    lane*/}

        {/*    <input type='checkbox'/>*/}
        {/*</label>*/}

        {/*<label>*/}
        {/*    items*/}

        {/*    <input type='checkbox'/>*/}
        {/*</label>*/}
    </div>
}

export default Settings;
