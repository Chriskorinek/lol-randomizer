import itemsResponse from './items.json';

export type ItemDef = {
    name: string;
    description: string;
    depth: number,
    into?: number[];
    image: {
        full: string,
    },
    gold: {
        purchasable: true,
        total: number
    },
    maps: {
        [id: string]: boolean
    }
}

const items = (Object.values(itemsResponse.data) as ItemDef[]).filter(i => i.maps['11'] && i.gold.purchasable) as ItemDef[]

export default () => items;
