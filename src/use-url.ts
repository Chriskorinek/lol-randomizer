import {useMemo} from "react";

export type ParamsType = {
    players: PlayerParamType[]
}

export type PlayerParamType = {
    number: number,
    items: number[],
    lane: string,
    champion: number
}

export default () => useMemo(() => {
    const params = new URLSearchParams(window.location.search);

    return undefined;
}, [])
