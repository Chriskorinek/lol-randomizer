import React, {useState} from 'react';
import './App.css';
import Settings, {SettingsDef} from "./settings";
import useChampions, {ChampionDef} from "./use-champions";
import {shuffle} from "./utils/array";
import useItems, {ItemDef} from "./use-items";

type RandomizedResult = any;

const lanes = ['top', 'jungle', 'mid', 'bottom', 'support']

const builds = ['fighter', 'marksman', 'assassin', 'mage', 'support']

function App() {
    const champions = useChampions();
    const allItems = useItems();
    const [results, setResults] = useState<RandomizedResult[] | null>(null)
    const [settings, setSettings] = useState<SettingsDef>({
        numberOfPlayers: 1,
        champions: true,
        items: true,
        lane: true
    })

    const randomize = () => {
        const shuffledChampions = shuffle(Object.values(champions));
        const shuffledLanes = shuffle(lanes);

        const newResults = [];

        for (let i = 0; i < settings.numberOfPlayers; i++) {
            const shuffledItems = shuffle(allItems) as ItemDef[];
            const mythicItem = shuffledItems.find(it => it.description.includes('<rarityMythic>') && !!it.into?.length);
            const items = shuffledItems.filter(it => !(it.description.includes('<rarityMythic>')) && it.gold.total > 2000).slice(0,4);

            newResults.push({
                champion: shuffledChampions[i],
                lane: shuffledLanes[i],
                build: shuffle(builds)[0],
                items: [mythicItem, ...items]
            })
        }

        setResults(newResults);
    }

    console.log(results)

    return (
        <div className="App">
            <Settings settings={settings} onChange={setSettings}/>

            <button onClick={randomize}>randomize</button>

            {results ? <div>
                <h2>results</h2>
                <div className="results">
                    {results.map((result, i) =>
                        <div className="results__player">
                            <h3>Player {i+1}</h3>
                            <div>
                                lane: <b>{result.lane}</b>
                            </div>
                            <img src={`http://ddragon.leagueoflegends.com/cdn/13.9.1/img/champion/${result.champion.image.full}`} />
                            <div>
                                {result.items.map((i: any) => <img src={`http://ddragon.leagueoflegends.com/cdn/13.9.1/img/item/${i?.image?.full}`} />)}
                            </div>
                        </div>
                    )}
                </div>
            </div> : null}
        </div>
    );
}

export default App;
