import championsResponse from './champion.json';

export type ChampionsDef = {
    [name: string]: ChampionsDef
}

export type ChampionDef = {
    name: string;
}

export default () => championsResponse.data
